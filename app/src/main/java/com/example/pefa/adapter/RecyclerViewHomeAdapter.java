package com.example.pefa.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.pefa.R;
import com.example.pefa.activities.SubCategoryActivity;
import com.example.pefa.model.Category;

import java.util.List;

public class RecyclerViewHomeAdapter extends RecyclerView.Adapter<RecyclerViewHomeAdapter.MyViewHolder>{
    private Context context;
    private List<Category> categoryList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView imageView;
        public View mView;

        public MyViewHolder(View view) {
            super(view);
            imageView = view.findViewById(R.id.imgCategory);
            mView = view;
        }
    }

    public RecyclerViewHomeAdapter(Context context, List<Category> categoryList) {
        this.context = context;
        this.categoryList = categoryList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_home_layout, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, final int position) {
        Category category = categoryList.get(position);
        myViewHolder.imageView.setImageResource(category.getImage());
        myViewHolder.mView.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), SubCategoryActivity.class);
                intent.putExtra("category", position + 1);
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return categoryList.size();
    }
}
