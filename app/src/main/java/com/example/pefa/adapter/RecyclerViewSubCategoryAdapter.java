package com.example.pefa.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.pefa.R;
import com.example.pefa.activities.ContentActivity;
import com.example.pefa.model.SubCategory;

import java.io.InputStream;
import java.util.List;

import static android.support.constraint.Constraints.TAG;

public class RecyclerViewSubCategoryAdapter extends RecyclerView.Adapter<RecyclerViewSubCategoryAdapter.MyViewHolder>{

    private Context context;
    private List<SubCategory> subCategoryList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView imageView;
        public TextView textView;
        public View mView;

        public MyViewHolder(View view) {
            super(view);
            imageView = view.findViewById(R.id.subImgCategory);
            textView = view.findViewById(R.id.subTextCategory);
            mView = view;
        }
    }

    public RecyclerViewSubCategoryAdapter(Context context, List<SubCategory> subCategoryList) {
        this.context = context;
        this.subCategoryList = subCategoryList;
    }

    @NonNull
    @Override
    public RecyclerViewSubCategoryAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.recycler_sub_layout, viewGroup, false);
        return new RecyclerViewSubCategoryAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerViewSubCategoryAdapter.MyViewHolder myViewHolder, final int i) {
        myViewHolder.textView.setText(subCategoryList.get(i).getSub_name());
        try{
            InputStream ims = context.getAssets().open(subCategoryList.get(i).getSub_image());
            Drawable drawable = Drawable.createFromStream(ims, null);
            myViewHolder.imageView.setImageDrawable(drawable);
            ims.close();
        } catch (Exception e) {
            Log.v(TAG, e.getMessage());
        }

        myViewHolder.mView.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ContentActivity.class);
                intent.putExtra("action", "from_subcategory");
                intent.putExtra("category_id", subCategoryList.get(i).getCategory_id());
                intent.putExtra("sub_id", subCategoryList.get(i).getSub_id());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return subCategoryList.size();
    }
}
