package com.example.pefa.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.pefa.R;
import com.example.pefa.model.SubCategory;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import static android.support.constraint.Constraints.TAG;

public class AutocompleteAdapter extends ArrayAdapter<SubCategory> {
    List<SubCategory> subCategoryList, tempItems, suggestions;
    Context context;

    public AutocompleteAdapter(Context context, List<SubCategory> subCategoryList) {
        super(context, android.R.layout.simple_list_item_1, subCategoryList);
        this.context = context;
        this.subCategoryList = subCategoryList;
        tempItems = new ArrayList<>(subCategoryList);
        suggestions = new ArrayList<>();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        SubCategory subCategory = subCategoryList.get(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.autocomplete_layout, parent, false);
        }
        TextView acText = convertView.findViewById(R.id.acText);
        ImageView acImage = convertView.findViewById(R.id.acImage);

        acText.setText(subCategory.getSub_name());
        try{
            InputStream ims = context.getAssets().open(subCategoryList.get(position).getSub_image());
            Drawable drawable = Drawable.createFromStream(ims, null);
            acImage.setImageDrawable(drawable);
            ims.close();
        } catch (Exception e) {
            Log.v(TAG, e.getMessage());
        }

        return convertView;
    }

    @Nullable
    @Override
    public SubCategory getItem(int position) {
        return subCategoryList.get(position);
    }
    @Override
    public int getCount() {
        return subCategoryList.size();
    }
    @Override
    public long getItemId(int position) {
        return position;
    }
    @NonNull
    @Override
    public Filter getFilter() {
        return subCategoryFilter;
    }

    private Filter subCategoryFilter = new Filter() {
        @Override
        public CharSequence convertResultToString(Object resultValue) {
            SubCategory subCategory = (SubCategory) resultValue;
            return subCategory.getSub_name();
        }

        @Override
        protected FilterResults performFiltering(CharSequence charSequence) {
            if (charSequence != null) {
                suggestions.clear();
                for (SubCategory subCategory: tempItems) {
                    if (subCategory.getSub_name().toLowerCase().startsWith(charSequence.toString().toLowerCase())) {
                        suggestions.add(subCategory);
                    }
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = suggestions;
                filterResults.count = suggestions.size();
                return filterResults;
            } else {
                return new FilterResults();
            }
        }
        @Override
        protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
            ArrayList<SubCategory> tempValues = (ArrayList<SubCategory>) filterResults.values;
            if (filterResults != null && filterResults.count > 0) {
                clear();
                for (SubCategory subCategoryObj : tempValues) {
                    add(subCategoryObj);
                    notifyDataSetChanged();
                }
            } else {
                clear();
                notifyDataSetChanged();
            }
        }
    };
}
