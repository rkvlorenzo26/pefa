package com.example.pefa.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.pefa.R;
import com.example.pefa.adapter.RecyclerViewSubCategoryAdapter;
import com.example.pefa.database.DatabaseBookmark;
import com.example.pefa.model.SubCategory;

import java.util.ArrayList;
import java.util.List;


public class FragmentBookmark extends Fragment {
    private DatabaseBookmark db;
    RecyclerView recyclerView;
    RecyclerViewSubCategoryAdapter recyclerViewSubCategoryAdapter;
    private List<SubCategory> subList = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_bookmark, container,false);

        db = new DatabaseBookmark(getActivity());

        subList.clear();
        subList = db.getAllBookmarks();
        recyclerView = rootView.findViewById(R.id.rvBookmarks);
        recyclerViewSubCategoryAdapter = new RecyclerViewSubCategoryAdapter(getActivity(), subList);
        GridLayoutManager manager = new GridLayoutManager(getActivity(), 2, GridLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(manager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(recyclerViewSubCategoryAdapter);

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        subList.clear();
        subList.addAll(db.getAllBookmarks());
        recyclerViewSubCategoryAdapter.notifyDataSetChanged();
    }
}
