package com.example.pefa.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;

import com.example.pefa.R;
import com.example.pefa.activities.ContentActivity;
import com.example.pefa.adapter.AutocompleteAdapter;
import com.example.pefa.adapter.RecyclerViewHomeAdapter;
import com.example.pefa.database.DatabaseHelper;
import com.example.pefa.model.Category;
import com.example.pefa.model.SubCategory;

import java.util.ArrayList;
import java.util.List;

public class FragmentHome extends Fragment {
    private DatabaseHelper db;
    RecyclerView recyclerView;
    RecyclerViewHomeAdapter recyclerViewHomeAdapter;
    private List<Category> categoryList = new ArrayList<>();
    AutoCompleteTextView autocompleteSearch;
    AutocompleteAdapter adapter = null;
    private List<SubCategory> subList = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_home, container,false);

        db = new DatabaseHelper(getActivity());
        subList.clear();
        subList = db.getAllSubCategory();
        autocompleteSearch = rootView.findViewById(R.id.autocompleteSearch);
        adapter = new AutocompleteAdapter(getActivity(), subList);
        autocompleteSearch.setThreshold(1);
        autocompleteSearch.setAdapter(adapter);

        // handle click event and set desc on textview
        autocompleteSearch.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                SubCategory subCategory = (SubCategory) adapterView.getItemAtPosition(i);
                Intent intent = new Intent(getActivity(), ContentActivity.class);
                intent.putExtra("action", "from_subcategory");
                intent.putExtra("category_id", subCategory.getCategory_id());
                intent.putExtra("sub_id", subCategory.getSub_id());
                startActivity(intent);
            }
        });


        recyclerView = rootView.findViewById(R.id.rvHome);
        recyclerViewHomeAdapter = new RecyclerViewHomeAdapter(getActivity(), categoryList);
        GridLayoutManager manager = new GridLayoutManager(getActivity(), 2, GridLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(manager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(recyclerViewHomeAdapter);

        loadCategory();

        return rootView;
    }

    private void loadCategory() {

        Integer[] images = {
                R.drawable.home1, R.drawable.home2,
                R.drawable.home3, R.drawable.home4,
                R.drawable.home5, R.drawable.home6,
                R.drawable.home7, R.drawable.home8,
                R.drawable.home9, R.drawable.home10,
                R.drawable.home11, R.drawable.home12,
        };

        for (int i = 0; i < images.length; i++) {
            Category category = new Category();
            category.setImage(images[i]);

            categoryList.add(category);
            recyclerViewHomeAdapter = new RecyclerViewHomeAdapter(getActivity(), categoryList);
        }

        recyclerViewHomeAdapter.notifyDataSetChanged();
    }

}
