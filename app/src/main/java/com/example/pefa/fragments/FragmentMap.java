package com.example.pefa.fragments;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.pefa.R;
import com.example.pefa.activities.MainActivity;
import com.example.pefa.model.Hospital;
import com.example.pefa.model.Map;
import com.example.pefa.utils.LoadingDialog;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class FragmentMap extends Fragment implements OnMapReadyCallback {
    MapView mMapView;
    private GoogleMap mMap;
    private List<Map> mapList;
    private List<Hospital> hospitalList;

    private ProgressDialog mProgress;
    private AlertDialog.Builder builder;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_map, container,false);
        mMapView = rootView.findViewById(R.id.mapView);
        mMapView.onCreate(savedInstanceState);

        mMapView.onResume();// needed to get the map to display immediately

        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        mMapView.getMapAsync(this);
        return rootView;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        LatLng pos = new LatLng(MainActivity.latitude, MainActivity.longitude);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(pos, 13.0f));

        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            mMap.setMyLocationEnabled(true);
        }
        requestToGoogle(googleMap);
    }

    private void requestToGoogle(final GoogleMap googleMap) {
        builder = new AlertDialog.Builder(getContext());
        mProgress = new LoadingDialog(getContext()).generateDialog("Loading resources...");
        mProgress.show();

        StringBuilder sb = new StringBuilder()
                .append("https://maps.googleapis.com/maps/api/place/nearbysearch/json?")
                .append("location=").append(MainActivity.latitude).append(",").append(MainActivity.longitude)
                .append("&radius=10000&type=hospital")
                .append("&key=AIzaSyBxDBbmvJEyWwZ-aYdDAPJyxRtaWCB4tho");

        StringRequest stringReq = new StringRequest(Request.Method.GET, sb.toString(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try{
                    JSONObject obj = new JSONObject(response);
                    String status = obj.getString("status");
                    if (status.equals("OK")) {
                        hospitalList = new ArrayList<>();
                        JSONArray jsonarray = new JSONArray(obj.getString("results"));
                        for (int i = 0; i < jsonarray.length(); i++) {
                            JSONObject result = jsonarray.getJSONObject(i);
                            JSONObject geometry = new JSONObject(result.getString("geometry"));
                            JSONObject location = new JSONObject(geometry.getString("location"));

                            Hospital hospital = new Hospital();
                            hospital.setName(result.getString("name"));
                            hospital.setLatitude(location.getString("lat"));
                            hospital.setLongitude(location.getString("lng"));
                            hospitalList.add(hospital);
                        }

                        for (Hospital hospital : hospitalList) {
                            LatLng position = new LatLng(Double.parseDouble(hospital.getLatitude()), Double.parseDouble(hospital.getLongitude()));
                            googleMap.addMarker(new MarkerOptions()
                                    .position(position)
                                    .title(hospital.getName()));
                        }

                        mProgress.dismiss();
                    } else if (status.equals("ZERO_RESULTS")) {
                        builder.setMessage("No hospital nearby.").setNegativeButton("Okay", null).create().show();
                        mProgress.dismiss();
                    } else {
                        builder.setMessage("Unable to load resources.").setNegativeButton("Okay", null).create().show();
                        mProgress.dismiss();
                    }
                }catch (JSONException e){
                    e.printStackTrace();
                }
            }
        },new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                builder.setMessage("Unable to load resources.").setNegativeButton("Okay", null).create().show();
                mProgress.dismiss();
            }
        });

        RequestQueue queue = Volley.newRequestQueue(getContext());
        queue.add(stringReq);
    }


}
