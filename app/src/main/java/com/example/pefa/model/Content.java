package com.example.pefa.model;

public class Content {
    private int content_id;
    private String content;
    private String type;
    private int sub_id;

    public int getContent_id() {
        return content_id;
    }

    public void setContent_id(int content_id) {
        this.content_id = content_id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getSub_id() {
        return sub_id;
    }

    public void setSub_id(int sub_id) {
        this.sub_id = sub_id;
    }

    @Override
    public String toString() {
        return "Content{" +
                "content_id=" + content_id +
                ", content='" + content + '\'' +
                ", type='" + type + '\'' +
                ", sub_id=" + sub_id +
                '}';
    }
}
