package com.example.pefa.model;

public class SubCategory {
    private int sub_id;
    private String sub_name;
    private int category_id;
    private String sub_image;

    public int getSub_id() {
        return sub_id;
    }

    public void setSub_id(int sub_id) {
        this.sub_id = sub_id;
    }

    public String getSub_name() {
        return sub_name;
    }

    public void setSub_name(String sub_name) {
        this.sub_name = sub_name;
    }

    public int getCategory_id() {
        return category_id;
    }

    public void setCategory_id(int category_id) {
        this.category_id = category_id;
    }

    public String getSub_image() {
        return sub_image;
    }

    public void setSub_image(String sub_image) {
        this.sub_image = sub_image;
    }

    @Override
    public String toString() {
        return "SubCategory{" +
                "sub_id=" + sub_id +
                ", sub_name='" + sub_name + '\'' +
                ", category_id=" + category_id +
                ", sub_image=" + sub_image +
                '}';
    }
}
