package com.example.pefa.utils;

import android.app.ProgressDialog;
import android.content.Context;

public class LoadingDialog {
    private Context mContext;

    public LoadingDialog(Context mContext) {
        this.mContext = mContext;
    }

    public ProgressDialog generateDialog(String message) {
        ProgressDialog mProgress = new ProgressDialog(mContext);
        mProgress.setMessage(message);
        mProgress.setCancelable(false);
        mProgress.setIndeterminate(true);
        return mProgress;
    }
}
