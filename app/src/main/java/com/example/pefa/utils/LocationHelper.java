package com.example.pefa.utils;

import android.content.Context;
import android.location.LocationManager;

public class LocationHelper {
    private Context mContext;

    public LocationHelper(Context mContext) {
        this.mContext = mContext;
    }

    public boolean statusCheck() {
        final LocationManager manager = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);
        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            return false;
        }
        return true;
    }
}
