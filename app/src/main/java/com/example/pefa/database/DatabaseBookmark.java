package com.example.pefa.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.pefa.model.SubCategory;

import java.util.ArrayList;
import java.util.List;

public class DatabaseBookmark extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "db_bookmark";
    private static final int DATABASE_VERSION = 1;
    private static final String TABLE_NAME = "tbl_bookmarks";
    private static final String COLUMN_ID = "id";
    private static final String COLUMN_SUB_ID = "sub_id";
    private static final String COLUMN_NAME = "name";
    private static final String COLUMN_IMAGE = "image";
    private static final String COLUMN_DATE= "bookmark_date";

    public DatabaseBookmark(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME + "("
                + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + COLUMN_SUB_ID + " INTEGER,"
                + COLUMN_NAME + " TEXT,"
                + COLUMN_IMAGE + " TEXT,"
                + COLUMN_DATE + " DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP )";

        db.execSQL(CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String sql = "DROP TABLE IF EXISTS " + TABLE_NAME + ";";
        db.execSQL(sql);
        onCreate(db);
    }

    public List<SubCategory> getAllBookmarks () {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery("SELECT * FROM tbl_bookmarks", null);
        List<SubCategory> subList = new ArrayList<>();

        if(c.moveToFirst())
        {
            do
            {
                SubCategory subCategory = new SubCategory();
                subCategory.setSub_id(Integer.parseInt(c.getString(1)));
                subCategory.setSub_name(c.getString(2));
                subCategory.setSub_image(c.getString(3));
                subList.add(subCategory);
            } while(c.moveToNext());
        }
        c.close();
        db.close();

        return subList;
    }

    public boolean checkBookmark(int sub_id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery("SELECT * FROM tbl_bookmarks WHERE sub_id = ?", new String[]{String.valueOf(sub_id)});
        int ctr = 0;

        if(c.moveToFirst()) {
            do {
                ctr = ctr + 1;
            } while(c.moveToNext());
        }
        return ctr > 0;
    }

    public boolean saveBookmark(SubCategory subCategory) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_SUB_ID, subCategory.getSub_id());
        contentValues.put(COLUMN_NAME, subCategory.getSub_name());
        contentValues.put(COLUMN_IMAGE, subCategory.getSub_image());
        return db.insert(TABLE_NAME, null, contentValues) != -1;
    }

    public boolean removeBookmark(Integer sub_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(TABLE_NAME, "sub_id = ?", new String[]{String.valueOf(sub_id)}) > 0;
    }
}
