package com.example.pefa.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.pefa.model.Category;
import com.example.pefa.model.Content;
import com.example.pefa.model.SubCategory;
import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

import java.util.ArrayList;
import java.util.List;

public class DatabaseHelper extends SQLiteAssetHelper {

    private static final String TAG = "DatabaseHelper";
    private static final String DATABASE_NAME = "db_pefa.db";
    private static final int DATABASE_VERSION = 1;

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public SubCategory getSubCategoryBySubCategoryId(int sub_id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery("SELECT * FROM tbl_sub_categories where sub_id = ?", new String[]{String.valueOf(sub_id)});
        SubCategory subCategory = new SubCategory();

        if (c.moveToFirst()) {
            subCategory.setSub_id(Integer.parseInt(c.getString(0)));
            subCategory.setSub_name(c.getString(1));
            subCategory.setSub_image(c.getString(3));
        }

        c.close();
        db.close();
        return subCategory;
    }

    public List<SubCategory> getSubCategoryByCategoryId (int category_id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery("SELECT * FROM tbl_sub_categories where category_id = ?",new String[]{String.valueOf(category_id)});
        List<SubCategory> subList = new ArrayList<>();

        if(c.moveToFirst())
        {
            do
            {
                SubCategory subCategory = new SubCategory();
                subCategory.setSub_id(Integer.parseInt(c.getString(0)));
                subCategory.setSub_name(c.getString(1));
                subCategory.setCategory_id(category_id);
                subCategory.setSub_image(c.getString(3));

                subList.add(subCategory);
                Log.v(TAG, "" + subCategory);
            } while(c.moveToNext());
        }
        c.close();
        db.close();
        return subList;
    }

    public List<SubCategory> getAllSubCategory () {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery("SELECT * FROM tbl_sub_categories", null);
        List<SubCategory> subList = new ArrayList<>();

        if(c.moveToFirst())
        {
            do
            {
                SubCategory subCategory = new SubCategory();
                subCategory.setSub_id(Integer.parseInt(c.getString(0)));
                subCategory.setSub_name(c.getString(1));
                subCategory.setCategory_id(0);
                subCategory.setSub_image(c.getString(3));

                subList.add(subCategory);
                Log.v(TAG, "" + subCategory);
            } while(c.moveToNext());
        }
        c.close();
        db.close();
        return subList;
    }

    public Category getCategoryById (int category_id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery("SELECT * FROM tbl_categories where category_id = ?",new String[]{String.valueOf(category_id)});
        Category category = new Category();

        if(c.moveToFirst())
        {
            category.setCategory_id(Integer.parseInt(c.getString(0)));
            category.setCategory_name(c.getString(1));
        }
        c.close();
        db.close();
        return category;
    }

    public Content getContentIntroBySubId(int sub_id) {
        String type = "intro";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery("SELECT * FROM tbl_contents WHERE sub_id = ? AND type = ?",new String[]{String.valueOf(sub_id), type});
        Content content = new Content();

        if(c.moveToFirst())
        {
            content.setContent_id(Integer.parseInt(c.getString(0)));
            content.setContent(c.getString(1));
            content.setType(c.getString(2));
            content.setSub_id(Integer.parseInt(c.getString(3)));
        }
        c.close();
        db.close();

        return content;
    }

    public List<Content> getContentSubsById(int sub_id) {
        String type = "sub";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery("SELECT * FROM tbl_contents WHERE sub_id = ? AND type = ? ORDER BY content_id ASC",new String[]{String.valueOf(sub_id), type});
        List<Content> contentList = new ArrayList<>();

        if(c.moveToFirst())
        {
            do
            {
                Content content = new Content();
                content.setContent_id(Integer.parseInt(c.getString(0)));
                content.setContent(c.getString(1));
                content.setType(c.getString(2));
                content.setSub_id(Integer.parseInt(c.getString(3)));
                contentList.add(content);
            } while(c.moveToNext());
        }
        c.close();
        db.close();
        return contentList;
    }

    public List<Content> getContentLinksById(int sub_id) {
        String type = "link";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery("SELECT * FROM tbl_contents WHERE sub_id = ? AND type = ? ORDER BY content_id ASC",new String[]{String.valueOf(sub_id), type});
        List<Content> contentList = new ArrayList<>();

        if(c.moveToFirst())
        {
            do
            {
                Content content = new Content();
                content.setContent_id(Integer.parseInt(c.getString(0)));
                content.setContent(c.getString(1));
                content.setType(c.getString(2));
                content.setSub_id(Integer.parseInt(c.getString(3)));
                contentList.add(content);
            } while(c.moveToNext());
        }
        c.close();
        db.close();
        return contentList;
    }

    public List<Content> getContentVideosById(int sub_id) {
        String type = "video";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery("SELECT * FROM tbl_contents WHERE sub_id = ? AND type = ? ORDER BY content_id ASC",new String[]{String.valueOf(sub_id), type});
        List<Content> contentList = new ArrayList<>();

        if(c.moveToFirst())
        {
            do
            {
                Content content = new Content();
                content.setContent_id(Integer.parseInt(c.getString(0)));
                content.setContent(c.getString(1));
                content.setType(c.getString(2));
                content.setSub_id(Integer.parseInt(c.getString(3)));
                contentList.add(content);
            } while(c.moveToNext());
        }
        c.close();
        db.close();
        return contentList;
    }

}
