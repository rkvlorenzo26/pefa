package com.example.pefa.activities;

import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.example.pefa.R;
import com.example.pefa.database.DatabaseBookmark;
import com.example.pefa.database.DatabaseHelper;
import com.example.pefa.model.Content;
import com.example.pefa.model.SubCategory;

import java.io.InputStream;
import java.util.List;

import static android.support.constraint.Constraints.TAG;

public class ContentActivity extends AppCompatActivity {

    private DatabaseHelper db;
    private DatabaseBookmark bookmark;

    FloatingActionButton floatingBookmark;
    ImageView imgContent;
    TextView txtContent, txtIntro ,txtSub;

    boolean isBookmarked = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_content);

        db = new DatabaseHelper(this);
        bookmark = new DatabaseBookmark(this);

        SubCategory subCategory = db.getSubCategoryBySubCategoryId(getIntent().getExtras().getInt("sub_id"));
        Content content = db.getContentIntroBySubId(getIntent().getExtras().getInt("sub_id"));
        loadTitle(subCategory);
        initializeUI(subCategory, content);
    }

    private String loadSubContents() {
        List<Content> subContentList = db.getContentSubsById(getIntent().getExtras().getInt("sub_id"));
        StringBuilder sb = new StringBuilder();

        if (subContentList.size() != 0) {
            for (Content content : subContentList) {
                sb.append("<p>• ").append(content.getContent()).append("</p>");
            }
        }

        List<Content> linkContentList = db.getContentLinksById(getIntent().getExtras().getInt("sub_id"));
        if (linkContentList.size() != 0) {
            sb.append("<p>Source: ").append(linkContentList.get(0).getContent()).append("</p>");
        }

        return sb.toString();
    }

    private String loadVideoContents() {
        List<Content> videoContentList = db.getContentVideosById(getIntent().getExtras().getInt("sub_id"));
        if (videoContentList.size() != 0) {
            return videoContentList.get(0).getContent();
        }
        return "";
    }

    private void initializeUI(SubCategory subCategory, Content content) {
        imgContent = findViewById(R.id.imgContent);
        txtContent = findViewById(R.id.txtContent);
        txtIntro = findViewById(R.id.txtIntro);
        txtSub = findViewById(R.id.txtSub);
        floatingBookmark = findViewById(R.id.floatingBookmark);

        txtContent.setText(subCategory.getSub_name());

        String videoName = loadVideoContents();

        final VideoView videoView = findViewById(R.id.videoView);

        if (videoName.isEmpty()) {
            videoView.setVisibility(View.GONE);
        } else {
            String path = "android.resource://" + getPackageName() + "/" + "raw/" + videoName;
            videoView.setVideoURI(Uri.parse(path));
            videoView.start();
            videoView.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if (videoView.isPlaying()) {
                        videoView.pause();
                    } else {
                        videoView.start();
                    }
                    return true;
                }
            });
        }

        String subContents = loadSubContents();

        if (content.getContent() != null) {
            txtIntro.setText(Html.fromHtml("<p>" + content.getContent() + "</p>" + subContents));
        } else {
            txtIntro.setText(Html.fromHtml(subContents));
        }

        try{
            InputStream ims = this.getAssets().open(subCategory.getSub_image());
            Drawable drawable = Drawable.createFromStream(ims, null);
            imgContent.setImageDrawable(drawable);
            ims.close();
        } catch (Exception e) {
            Log.v(TAG, e.getMessage());
        }

        if (bookmark.checkBookmark(getIntent().getExtras().getInt("sub_id"))) {
            floatingBookmark.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.materialPrimaryDark)));
            isBookmarked = true;
        }
    }

    public void saveBookmark(View view) {
        if (isBookmarked) {
            if (bookmark.removeBookmark(getIntent().getExtras().getInt("sub_id"))) {
                floatingBookmark.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.materialAccent)));
                isBookmarked = false;
            }
        } else {
            SubCategory subCategory = db.getSubCategoryBySubCategoryId(getIntent().getExtras().getInt("sub_id"));
            if (bookmark.saveBookmark(subCategory)) {
                floatingBookmark.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.materialPrimaryDark)));
                Snackbar.make(view, "Successfully saved in bookmarks.",
                        Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                isBookmarked = true;
            }
        }
    }

    private void loadTitle(SubCategory subCategory) {
        assert getSupportActionBar() != null;   //null check
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);   //show back button
        setTitle(subCategory.getSub_name().toUpperCase());    //set Title
    }

    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }
}
