package com.example.pefa.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.pefa.R;
import com.example.pefa.adapter.RecyclerViewSubCategoryAdapter;
import com.example.pefa.database.DatabaseHelper;
import com.example.pefa.model.Category;
import com.example.pefa.model.SubCategory;

import java.util.ArrayList;
import java.util.List;

public class SubCategoryActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    RecyclerViewSubCategoryAdapter recyclerViewSubCategoryAdapter;
    private List<SubCategory> subList = new ArrayList<>();
    private DatabaseHelper db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub_category);

        db = new DatabaseHelper(this);

        loadTitle(getIntent().getExtras().getInt("category"));
        initializeUI();
        loadData(getIntent().getExtras().getInt("category"));
    }

    private void initializeUI() {
        recyclerView = findViewById(R.id.rvSubCategory);
        recyclerViewSubCategoryAdapter = new RecyclerViewSubCategoryAdapter(this, subList);
        GridLayoutManager manager = new GridLayoutManager(this, 2, GridLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(manager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(recyclerViewSubCategoryAdapter);
    }

    private void loadData(int category_id) {
        subList.addAll(db.getSubCategoryByCategoryId(category_id));
        recyclerViewSubCategoryAdapter = new RecyclerViewSubCategoryAdapter(this, subList);
        recyclerViewSubCategoryAdapter.notifyDataSetChanged();
    }

    private void loadTitle(int category_id) {
        Category category = db.getCategoryById(category_id);
        assert getSupportActionBar() != null;   //null check
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);   //show back button
        setTitle(category.getCategory_name().toUpperCase());    //set Title
    }

    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }
}
